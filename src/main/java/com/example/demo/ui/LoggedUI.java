package com.example.demo.ui;

import com.example.demo.model.*;

import com.example.demo.repository.*;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class LoggedUI extends VerticalLayout {

    private MessageRepository messageRepository;
    private UserRepository userRepository;
    private BookRepository bookRepository;
    private RequestRepository requestRepository;

    private User user;
    private VerticalLayout verticalLayout;

    private VerticalLayout messagesLayoutA;
    private VerticalLayout requestLayoutA = new VerticalLayout();
    private VerticalLayout booksListLayoutA = new VerticalLayout();
    private VerticalLayout booksEditLayoutA = new VerticalLayout();
    private VerticalLayout usersLayoutA = new VerticalLayout();

    private VerticalLayout messagesLayoutR;
    private VerticalLayout userBooksLayoutR;
    private VerticalLayout freeBooksLayoutR;
    private VerticalLayout borrowBookLayoutR;

    Button borrowBook;
    Button requests;
    Button booksEdit;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static LocalDate lastDate = LocalDate.now().minusDays(1);

    public LoggedUI(User user, MessageRepository messageRepository, UserRepository userRepository,
                    BookRepository bookRepository, RequestRepository requestRepository) {

        this.user = user;
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
        this.requestRepository = requestRepository;

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        verticalLayout = new VerticalLayout();

        if (user.getType().equals(Type.LIBRARIAN)) {
            LocalDate newDate = LocalDate.now();

            if (!newDate.equals(lastDate)) {
                sendReminds();
                lastDate = newDate;
            }

            Button messages = new Button("Powiadomienia");
            messages.addClickListener(event -> {
                refreshAdminLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(messagesLayoutA);
            });

            requests = new Button("Zgłoszenia");
            requests.addClickListener(event -> {
                refreshAdminLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(requestLayoutA);
            });

            Button booksList = new Button("Lista książek");
            booksList.addClickListener(event -> {
                refreshAdminLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(booksListLayoutA);
            });

            booksEdit = new Button("Edycja książek");
            booksEdit.addClickListener(event -> {
                refreshAdminLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(booksEditLayoutA);
            });

            Button users = new Button("Czytelnicy");
            users.addClickListener(event -> {
                refreshAdminLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(usersLayoutA);
            });

            messages.click();
            horizontalLayout.addComponents(messages, requests, booksList, booksEdit, users);
            addComponents(horizontalLayout, verticalLayout);
        }
        else if (user.getType().equals(Type.READER)) {

            Button messages = new Button("Powiadomienia");
            messages.addClickListener(event -> {
                refreshReaderLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(messagesLayoutR);
            });

            Button userBooks = new Button("Wypożyczone książki");
            userBooks.addClickListener(event -> {
                refreshReaderLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(userBooksLayoutR);
            });

            Button freeBooks = new Button("Dostępne książki");
            freeBooks.addClickListener(event -> {
                refreshReaderLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(freeBooksLayoutR);
            });

            borrowBook = new Button("Wypożyczenie");
            borrowBook.addClickListener(event -> {
                refreshReaderLayout();
                verticalLayout.removeAllComponents();
                verticalLayout.addComponent(borrowBookLayoutR);
            });

            messages.click();
            horizontalLayout.addComponents(messages, userBooks, freeBooks, borrowBook);
            addComponents(horizontalLayout, verticalLayout);
        }
    }

    private void sendReminds() {
        requestRepository.findAll()
                .stream()
                .filter(r -> r.getReturnDate() != null)
                .filter(r -> ChronoUnit.DAYS.between(LocalDate.now(), r.getReturnDate()) == 5)
                .forEach(r -> {
                    Message m = new Message(0L, "Przypomnienie", "Do oddania książki - "
                            + r.getBook().getTitle() + " - zostało 5 dni!", LocalDateTime.now(), r.getUser());
                    messageRepository.save(m);
                });

        requestRepository.findAll()
                .stream()
                .filter(r -> r.getReturnDate() != null)
                .filter(r -> ChronoUnit.DAYS.between(LocalDate.now(), r.getReturnDate()) == 1)
                .forEach(r -> {
                    Message m = new Message(0L, "Przypomnienie", "Jutro mija termin oddania książki - "
                            + r.getBook().getTitle() + "!", LocalDateTime.now(), r.getUser());
                    messageRepository.save(m);
                });

        requestRepository.findAll()
                .stream()
                .filter(r -> r.getReturnDate() != null)
                .filter(r -> ChronoUnit.DAYS.between(LocalDate.now(), r.getReturnDate()) == 0)
                .forEach(r -> {
                    Message m = new Message(0L, "Powiadomienie", "Minął termin oddania książki - "
                            + r.getBook().getTitle() + "!", LocalDateTime.now(), r.getUser());
                    messageRepository.save(m);
                });
    }

    private void refreshAdminLayout() {
        initMessagesLayoutA();
        initRequestLayoutA();
        initBooksListLayoutA();
        initBooksEditLayoutA();
        initUsersLayoutA();
    }

    private void initMessagesLayoutA() {
        messagesLayoutA = new VerticalLayout();

        List<User> users = userRepository.findAll()
                .stream()
                .filter(u -> u.getType() == Type.READER)
                .collect(Collectors.toList());

        ComboBox<User> userComboBox = new ComboBox<>("Wybierz czytelnika");
        userComboBox.setItems(users);
        userComboBox.setItemCaptionGenerator(user -> "(" + user.getId() + ") " + user.getLogin()
                + " - " + user.getFirstName() + " " + user.getSurname());
        userComboBox.setWidth("300");
        userComboBox.setEmptySelectionAllowed(false);

        Grid<Message> messageGrid = new Grid<>();
        messageGrid.addColumn(
                message -> message.getDateTime().format(formatter))
                                                .setCaption("Data i czas").setWidth(200);
        messageGrid.addColumn(Message::getTopic).setCaption("Temat").setWidth(200);
        messageGrid.addColumn(Message::getText).setCaption("Treść").setWidth(800);
        messageGrid.setWidth("1200");

        userComboBox.addValueChangeListener(event -> {
            List<Message> messages = messageRepository.findAllByUserOrderByDateTimeDesc(event.getValue());
            messageGrid.setItems(messages);
        });

        Button sendMessage = new Button("Wyślij powiadomienie");
        sendMessage.addClickListener(event -> {
            if(userComboBox.getValue() != null) {
                initWindow(userComboBox.getValue());
                userComboBox.setValue(null);
            }
            else {
                Notification.show("Błąd!", "Nie wybrano czytelnika", Notification.Type.ERROR_MESSAGE);
            }
        });

        messagesLayoutA.addComponents(userComboBox, messageGrid, sendMessage);
    }

    private void initWindow(User user) {
        Window windowAddNotification = new Window("Powiadomienie dla: " + user.getLogin()
                + " - " + user.getFirstName() + " " + user.getSurname());
        windowAddNotification.setWidth(400.0f, Sizeable.Unit.PIXELS);
        windowAddNotification.setModal(true);
        windowAddNotification.setResizable(false);
        windowAddNotification.center();
        windowAddNotification.setDraggable(true);

        FormLayout formWindowNotification = new FormLayout();
        formWindowNotification.setMargin(true);

        TextField topic = new TextField("Temat");
        TextArea text = new TextArea("Treść");

        Button send = new Button("Wyślij");
        send.addClickListener(event -> {
            if (topic.getValue().equals(""))
                Notification.show("Błąd!", "Nie podano tematu", Notification.Type.ERROR_MESSAGE);
            else if (text.getValue().equals(""))
                Notification.show("Błąd!","Nie podano treści", Notification.Type.ERROR_MESSAGE);
            else {
                Message message = new Message(0L, topic.getValue(), text.getValue(), LocalDateTime.now(), user);
                messageRepository.save(message);

                Notification.show("Sukces", "Powiadomienie zostało wysłane", Notification.Type.HUMANIZED_MESSAGE);
                topic.clear();
                text.clear();
            }
        });

        formWindowNotification.addComponents(topic, text, send);

        windowAddNotification.setContent(formWindowNotification);
        getUI().addWindow(windowAddNotification);
    }

    private void initRequestLayoutA() {
        requestLayoutA = new VerticalLayout();

        List<Request> requestList = requestRepository.findAll()
                .stream()
                .filter(r -> r.getPermission() == null)
                .collect(Collectors.toList());

        ComboBox<Request> requestsComboBox = new ComboBox<>("Wybierz zgłoszenie");
        requestsComboBox.setEmptySelectionAllowed(false);
        requestsComboBox.setItemCaptionGenerator(r -> "(" + r.getUser().getId() + ") " + r.getUser().getLogin()
                + " - " + r.getBook().getTitle() + " (" + r.getBook().getId() + ")");
        requestsComboBox.setWidth("450");
        requestsComboBox.setItems(requestList);

        Button allow = new Button("Potwierdź");
        allow.addClickListener(event1 -> {
            if (requestsComboBox.getValue() != null) {
                Request request = requestsComboBox.getValue();
                request.setPermission(true);
                request.setBorrowDate(LocalDate.now());
                request.setReturnDate(LocalDate.now().plusDays(30));
                requestRepository.save(request);

                Long id = requestsComboBox.getValue().getBook().getId();
                User user = requestsComboBox.getValue().getUser();
                Book book = bookRepository.findById(id).orElse(new Book());
                book.setUser(user);
                bookRepository.save(book);

                Message message = new Message(0L, "Wypożyczenie", "Wypożyczono książkę: "
                        + request.getBook().getTitle() + ". Okres wypożyczenia: 30 dni. Miłej lektury.", LocalDateTime.now(), user);
                messageRepository.save(message);

                requests.click();
                Notification.show("Sukces!", "Zgłoszenie zostało potwierdzone", Notification.Type.HUMANIZED_MESSAGE);
            } else
                Notification.show("Błąd!", "Nie wybrano zgłoszenia", Notification.Type.ERROR_MESSAGE);
        });

        Button refuse = new Button("Odrzuć");
        refuse.addClickListener(event1 -> {
            if (requestsComboBox.getValue() != null) {
                Request request = requestsComboBox.getValue();
                requestRepository.delete(request);

                Message message = new Message(0L, "Brak zgody", "Nie udzielono zgody na wypożyczenie książki: "
                        + request.getBook().getTitle() + ". Przepraszamy.", LocalDateTime.now(), requestsComboBox.getValue().getUser());
                messageRepository.save(message);

                requests.click();
                Notification.show("Sukces!", "Zgłoszenie zostało odrzucone", Notification.Type.HUMANIZED_MESSAGE);
            } else
                Notification.show("Błąd!", "Nie wybrano zgłoszenia", Notification.Type.ERROR_MESSAGE);
        });

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(allow, refuse);
        requestLayoutA.addComponents(requestsComboBox, horizontalLayout);
    }

    private void initBooksListLayoutA() {
        booksListLayoutA = new VerticalLayout();

        List<Book> allBooksList = bookRepository.findAll();

        Grid<Book> allBooksGrid = new Grid<>();
        allBooksGrid.addColumn(Book::getId).setCaption("ID").setWidth(60);
        allBooksGrid.addColumn(Book::getTitle).setCaption("Tytuł").setWidth(300);
        allBooksGrid.addColumn(Book::getAuthor).setCaption("Autor").setWidth(180);
        allBooksGrid.addColumn(Book::getPublishHouse).setCaption("Wydawnictwo").setWidth(200);
        allBooksGrid.addColumn(Book::getPages).setCaption("Strony").setWidth(80);
        allBooksGrid.addColumn(Book::getISBN).setCaption("ISBN").setWidth(180);
        allBooksGrid.addColumn(book ->
                requestRepository.findByBook(book) == null ? "-" :
                        bookRepository.findById(book.getId()).orElse(new Book()).getUser() == null ? "Z" :
                                book.getUser().getLogin()
                                        ).setCaption("Czytelnik").setWidth(150);
        allBooksGrid.addColumn(
                book -> book.getUser() == null ? "-" :
                    requestRepository.findByBook(book).getReturnDate()
                                        ).setCaption("Data oddania").setWidth(130);
        allBooksGrid.setWidth("1280");
        allBooksGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        allBooksGrid.setDataProvider(DataProvider.ofCollection(allBooksList));

        booksListLayoutA.addComponents(allBooksGrid);
    }

    private void initBooksEditLayoutA() {
        booksEditLayoutA = new VerticalLayout();

        TextField title = new TextField("Podaj tytuł");
        TextField author = new TextField("Podaj autora");
        TextField publishHouse = new TextField("Podaj wydawnictwo");
        TextField pages = new TextField("Podaj liczbę stron");
        TextField isbn = new TextField("Podaj nr ISBN");

        Button addButton = new Button("Dodaj książkę");
        addButton.addClickListener(event -> {
            if (!title.getValue().equals("") && !author.getValue().equals("") && !publishHouse.getValue().equals("")
                                                        && !pages.getValue().equals("") && !isbn.getValue().equals("")) {
                try {
                    Book book = new Book(0L, title.getValue(), author.getValue(), publishHouse.getValue(),
                            Integer.parseInt(pages.getValue()), isbn.getValue(), null);
                    bookRepository.save(book);

                    title.setValue("");
                    author.setValue("");
                    publishHouse.setValue("");
                    pages.setValue("");
                    isbn.setValue("");

                    Notification.show("Sukces!", "Książka została dodana", Notification.Type.HUMANIZED_MESSAGE);
                }
                catch(NumberFormatException e) {
                    Notification.show("Błąd!", "Nieprawidłowa liczba stron", Notification.Type.ERROR_MESSAGE);
                }
            } else
                Notification.show("Błąd!", "Nie podano wszystkich danych", Notification.Type.ERROR_MESSAGE);
        });

        Label label = new Label();

        List<Book> books = bookRepository.findAll()
                .stream()
                .filter(book -> book.getUser() != null)
                .collect(Collectors.toList());

        ComboBox<Book> bookComboBox = new ComboBox<>("Wybierz zwróconą książkę");
        bookComboBox.setEmptySelectionAllowed(false);
        bookComboBox.setItemCaptionGenerator(book -> "(" + book.getUser().getId() + ") " + book.getUser().getLogin()
                + " - " + book.getTitle() + " (" + book.getId() + ")");
        bookComboBox.setWidth("450");
        bookComboBox.setItems(books);

        Button returnBook = new Button("Odbierz książkę");
        returnBook.addClickListener(event1 -> {
            if (bookComboBox.getValue() != null) {
                Book book = bookRepository.findById(bookComboBox.getValue().getId()).orElse(new Book());
                book.setUser(null);
                bookRepository.save(book);

                Request request = requestRepository.findByBook(book);
                requestRepository.delete(request);

                Message message = new Message(0L, "Powiadomienie", "Książka: "
                        + book.getTitle() + " została zwrócona. Dziękujemy.", LocalDateTime.now(), bookComboBox.getValue().getUser());
                messageRepository.save(message);

                booksEdit.click();
                Notification.show("Sukces!", "Książka została odebrana", Notification.Type.HUMANIZED_MESSAGE);
            } else
                Notification.show("Błąd!", "Nie wybrano książki do odebrania", Notification.Type.ERROR_MESSAGE);
        });

        booksEditLayoutA.addComponents(title, author, publishHouse, pages, isbn, addButton, label, bookComboBox, returnBook);
    }

    private void initUsersLayoutA() {
        usersLayoutA = new VerticalLayout();

        List<User> users = userRepository.findAll()
                .stream()
                .filter(u -> u.getType() == Type.READER)
                .collect(Collectors.toList());

        Grid<User> usersGrid = new Grid<>();
        usersGrid.addColumn(User::getId).setCaption("ID").setWidth(60);
        usersGrid.addColumn(User::getLogin).setCaption("Login").setWidth(150);
        usersGrid.addColumn(User::getFirstName).setCaption("Imię").setWidth(150);
        usersGrid.addColumn(User::getSurname).setCaption("Nazwisko").setWidth(150);
        usersGrid.addColumn(
                user -> (long) bookRepository.findAllByUser(user).size())
                                            .setCaption("Książki").setWidth(80);
        usersGrid.setWidth("590");
        usersGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        usersGrid.setDataProvider(DataProvider.ofCollection(users));

        usersLayoutA.addComponents(usersGrid);
    }

    private void refreshReaderLayout() {
        initMessagesLayoutR();
        initBorrowedBooksLayoutR();
        initFreeBooksLayoutR();
        initBorrowBookLayoutR();
    }

    private void initMessagesLayoutR() {
        messagesLayoutR = new VerticalLayout();
        List<Message> messages = messageRepository.findAllByUserOrderByDateTimeDesc(user);

        Grid<Message> messageGrid = new Grid<>();
        messageGrid.addColumn(
                message -> message.getDateTime().format(formatter))
                                                .setCaption("Data i czas").setWidth(200);
        messageGrid.addColumn(Message::getTopic).setCaption("Temat").setWidth(200);
        messageGrid.addColumn(Message::getText).setCaption("Treść").setWidth(800);
        messageGrid.setWidth("1200");
        messageGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        messageGrid.setDataProvider(DataProvider.ofCollection(messages));

        messagesLayoutR.addComponents(messageGrid);
    }

    private void initBorrowedBooksLayoutR() {
        userBooksLayoutR = new VerticalLayout();

        List<Book> userBooksList = bookRepository.findAllByUser(user);

        Grid<Book> userBooksGrid = new Grid<>();
        userBooksGrid.addColumn(Book::getId).setCaption("ID").setWidth(60);
        userBooksGrid.addColumn(Book::getTitle).setCaption("Tytuł").setWidth(300);
        userBooksGrid.addColumn(Book::getAuthor).setCaption("Autor").setWidth(180);
        userBooksGrid.addColumn(Book::getPublishHouse).setCaption("Wydawnictwo").setWidth(200);
        userBooksGrid.addColumn(Book::getPages).setCaption("Strony").setWidth(80);
        userBooksGrid.addColumn(Book::getISBN).setCaption("ISBN").setWidth(180);
        userBooksGrid.addColumn(book ->
                requestRepository.findByBook(book).getBorrowDate())
                                                .setCaption("Data wypoż.").setWidth(130);
        userBooksGrid.addColumn(book ->
                requestRepository.findByBook(book).getReturnDate())
                                                .setCaption("Data oddania").setWidth(130);
        userBooksGrid.setWidth("1260");
        userBooksGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        userBooksGrid.setDataProvider(DataProvider.ofCollection(userBooksList));

        userBooksLayoutR.addComponents(userBooksGrid);
    }

    private void initFreeBooksLayoutR() {
        freeBooksLayoutR = new VerticalLayout();

        List<Book> freeBooksList = bookRepository.findAll()
                .stream()
                .filter(book -> requestRepository.findByBook(book) == null)
                .collect(Collectors.toList());

        Grid<Book> freeBooksGrid = new Grid<>();
        freeBooksGrid.addColumn(Book::getId).setCaption("ID").setWidth(60);
        freeBooksGrid.addColumn(Book::getTitle).setCaption("Tytuł").setWidth(300);
        freeBooksGrid.addColumn(Book::getAuthor).setCaption("Autor").setWidth(180);
        freeBooksGrid.addColumn(Book::getPublishHouse).setCaption("Wydawnictwo").setWidth(200);
        freeBooksGrid.addColumn(Book::getPages).setCaption("Strony").setWidth(80);
        freeBooksGrid.addColumn(Book::getISBN).setCaption("ISBN").setWidth(180);
        freeBooksGrid.setWidth("1000");
        freeBooksGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        freeBooksGrid.setDataProvider(DataProvider.ofCollection(freeBooksList));

        freeBooksLayoutR.addComponents(freeBooksGrid);
    }

    private void initBorrowBookLayoutR() {
        borrowBookLayoutR = new VerticalLayout();

        List<Book> freeBooksList = bookRepository.findAll()
                .stream()
                .filter(book -> requestRepository.findByBook(book) == null)
                .collect(Collectors.toList());

        ComboBox<Book> bookComboBox = new ComboBox<>("Wybierz książkę");
        bookComboBox.setEmptySelectionAllowed(false);
        bookComboBox.setItemCaptionGenerator(book -> "(" + book.getId() + ") " + book.getTitle()
                                        + " - " + book.getAuthor());
        bookComboBox.setWidth("500");
        bookComboBox.setDataProvider(DataProvider.ofCollection(freeBooksList));

        Button borrow = new Button("Wypożycz książkę");
        borrow.addClickListener(event1 -> {
            if (bookComboBox.getValue() != null) {
                Book book = bookComboBox.getValue();

                Request request = new Request(0L, null, null, null, user, book);
                requestRepository.save(request);

                Message message = new Message(0L, "Zgłoszenie", "Zgłoszono wypożyczenie książki: " + book.getTitle(), LocalDateTime.now(), user);
                messageRepository.save(message);

                borrowBook.click();
                Notification.show("Sukces", "Zgłoszenie zostało wysłane", Notification.Type.HUMANIZED_MESSAGE);
            } else
                Notification.show("Błąd!", "Nie wybrano książki", Notification.Type.ERROR_MESSAGE);
        });

        borrowBookLayoutR.addComponents(bookComboBox, borrow);
    }
}