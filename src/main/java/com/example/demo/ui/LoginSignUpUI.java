package com.example.demo.ui;

import com.example.demo.model.Type;
import com.example.demo.model.User;
import com.example.demo.repository.*;
import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringUI
@Theme("mytheme")
public class LoginSignUpUI extends UI {

    private MessageRepository messageRepository;
    private UserRepository userRepository;
    private BookRepository bookRepository;
    private RequestRepository requestRepository;
    private PasswordEncoder passwordEncoder;

    private VerticalLayout root;
    private VerticalLayout verticalLayout;

    private Button log;
    private Button reg;

    @Autowired
    public LoginSignUpUI(MessageRepository messageRepository,
                         UserRepository userRepository,
                         BookRepository bookRepository,
                         RequestRepository requestRepository,
                         PasswordEncoder passwordEncoder) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
        this.requestRepository = requestRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setStyleName("backgroundImage");
        Page.getCurrent().setTitle("Biblioteka online");

        root = new VerticalLayout();
        root.setSpacing(true);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        log = new Button("Logowanie");
        reg = new Button("Rejestracja");

        horizontalLayout.addComponents(log, reg);
        verticalLayout = new VerticalLayout();

        logging();
        registration();
        log.click();

        root.addComponents(horizontalLayout, verticalLayout);
        setContent(root);
    }

    private void registration() {
        reg.addClickListener(clickEvent -> {
            verticalLayout.removeAllComponents();

            TextField firstNameTextField = new TextField("Podaj imię: ");
            TextField surnameTextField = new TextField("Podaj nazwisko: ");
            TextField loginTextField = new TextField("Podaj login: ");
            PasswordField passwordField = new PasswordField("Podaj hasło: ");
            Label label = new Label();
            Button login = new Button("Zarejestruj się");

            verticalLayout.addComponents(firstNameTextField, surnameTextField, loginTextField, passwordField, label, login);

            login.addClickListener(event -> {
                String firstNameText = firstNameTextField.getValue();
                String surnameText = surnameTextField.getValue();
                String loginText = loginTextField.getValue();
                String passwordText = passwordField.getValue();

                if (firstNameText.length() > 3 && surnameText.length() > 3 && loginText.length() > 3) {
                    if (passwordText.length() > 5 && passwordText.length() < 20) {
                        if (checkPassword(passwordText)) {
                            if (!userRepository.findAllLogins().contains(loginText)) {

                                User user = new User(0L, loginText,
                                        passwordEncoder.encode(passwordText),
                                        Type.READER, firstNameText, surnameText);
                                userRepository.save(user);

                                Notification.show("Sukces!", "Rejestracja przebiegła pomyślnie", Notification.Type.HUMANIZED_MESSAGE);
                                log.click();
                            } else
                                Notification.show("Błąd!", "Podany login już istnieje", Notification.Type.ERROR_MESSAGE);
                        } else
                            Notification.show("Błąd!",
                                    "Hasło musi składać się z conajmniej: 1 małej litery, 1 dużej litery i 1 cyfry", Notification.Type.ERROR_MESSAGE);
                    } else
                        Notification.show("Błąd!", "Hasło musi składać się z conajmniej 6 znaków", Notification.Type.ERROR_MESSAGE);

                } else
                    Notification.show("Błąd!", "Dane muszą składać się z conajmniej 4 znaków", Notification.Type.ERROR_MESSAGE);
            });
        });
    }

    private void logging() {
        log.addClickListener(clickEvent -> {
            verticalLayout.removeAllComponents();

            TextField loginTextField = new TextField("Podaj login: ");
            PasswordField passwordField = new PasswordField("Podaj hasło: ");
            Label label = new Label();
            Button login = new Button("Zaloguj się");

            verticalLayout.addComponents(loginTextField, passwordField, label, login);

            login.addClickListener(event -> {
                Optional<User> user = userRepository.findByLogin(loginTextField.getValue());
                if (user.isPresent()) {
                    if (passwordEncoder.matches(passwordField.getValue(), user.get().getPassword())) {
                        Notification.show("Logowanie udane!", "", Notification.Type.HUMANIZED_MESSAGE);

                        root.removeAllComponents();
                        root.addComponent(new LoggedUI(user.get(), messageRepository,
                                userRepository, bookRepository, requestRepository));

                        Button logout = new Button("Wyloguj");
                        logout.addClickListener(event1 -> {
                            root.removeAllComponents();
                            init(null);
                        });

                        root.addComponent(logout);
                    } else {
                        Notification.show("Błąd!", "Nieprawidłowe hasło", Notification.Type.ERROR_MESSAGE);
                    }
                } else {
                    Notification.show("Błąd!", "Podany login nie istnieje", Notification.Type.ERROR_MESSAGE);
                }
            });
        });
    }

    private boolean checkPassword(String password) {
        final String patternString = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }
}