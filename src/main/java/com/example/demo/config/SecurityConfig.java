package com.example.demo.config;

import com.example.demo.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    public SecurityConfig(UserDetailsServiceImpl userDetailsServiceImpl) {
        this.userDetailsServiceImpl = userDetailsServiceImpl;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl)
                .passwordEncoder(getPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors()
                .and().authorizeRequests().antMatchers("/api/book/allFree").authenticated()
                .and().authorizeRequests().antMatchers("/api/book/**").hasRole("LIBRARIAN")
                .and().authorizeRequests().antMatchers("/api/request/add").authenticated()
                .and().authorizeRequests().antMatchers("/api/request/**").hasRole("LIBRARIAN")
                .and().authorizeRequests().antMatchers("/api/user/**").hasRole("LIBRARIAN")
                .and().authorizeRequests().antMatchers("/api/message/allByUser").authenticated()
                .and().authorizeRequests().antMatchers("/api/message/**").hasRole("LIBRARIAN")
                .and().formLogin().permitAll()
                .and().logout().permitAll()
                .and().httpBasic();
    }
}

