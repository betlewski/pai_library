package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/byLogin")
    @ResponseBody
    public User findByLogin(@RequestParam("login") String login) {
        return userService.findByLogin(login);
    }

    @GetMapping("/allLogins")
    @ResponseBody
    public List<String> findAllLogins() {
        return userService.findAllLogins();
    }

    @GetMapping("/allReaders")
    @ResponseBody
    public List<User> findAllReaders() {
        return userService.findAllReaders();
    }
}
