package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.model.User;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/book")
@CrossOrigin
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/allByUser")
    @ResponseBody
    public List<Book> findAllByUser(@RequestBody User user) {
        return bookService.findAllByUser(user);
    }

    @GetMapping("/allById")
    @ResponseBody
    public Book findById(@RequestParam("id") Long id) {
        return bookService.findById(id);
    }

    @GetMapping("/allFree")
    @ResponseBody
    public List<Book> findAllFree() {
        return bookService.findAllFree();
    }

    @PostMapping("/add")
    public ResponseEntity<Book> add(@RequestBody Book book) {
        return bookService.save(book);
    }
}
