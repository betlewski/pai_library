package com.example.demo.controller;

import com.example.demo.model.Message;
import com.example.demo.model.User;
import com.example.demo.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/message")
@CrossOrigin
public class MessageController {

    private MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/allByUser")
    @ResponseBody
    public List<Message> findAllByUserOrdered(@RequestBody User user) {
        return messageService.findAllByUserOrdered(user);
    }

    @PostMapping("/add")
    public ResponseEntity<Message> add(@RequestBody Message message) {
        return messageService.save(message);
    }
}
