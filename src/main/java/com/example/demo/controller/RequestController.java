package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.model.Request;
import com.example.demo.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/request")
@CrossOrigin
public class RequestController {

    private RequestService requestService;

    @Autowired
    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/byBook")
    @ResponseBody
    public Request findByBook(@RequestBody Book book) {
        return requestService.findByBook(book);
    }

    @GetMapping("/all")
    @ResponseBody
    public List<Request> findAll() {
        return requestService.findAll();
    }

    @PostMapping("/add")
    public ResponseEntity<Request> save(@RequestBody Request request) {
        return requestService.save(request);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Boolean> deleteById(@RequestParam("id") Long id) {
        return requestService.deleteById(id);
    }
}
