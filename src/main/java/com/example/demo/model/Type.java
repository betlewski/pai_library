package com.example.demo.model;

public enum Type {
    LIBRARIAN,
    READER;
}
