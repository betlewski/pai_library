package com.example.demo.service;

import com.example.demo.model.Type;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findByLogin(String login) {
        return userRepository.findByLogin(login).orElse(null);
    }

    public List<String> findAllLogins() {
        return userRepository.findAllLogins();
    }

    public List<User> findAllReaders() {
        return userRepository.findAll()
                .stream()
                .filter(u -> u.getType() == Type.READER)
                .collect(Collectors.toList());
    }
}
