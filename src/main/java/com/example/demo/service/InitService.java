package com.example.demo.service;

import com.example.demo.model.*;
import com.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@Service
public class InitService {

    private MessageRepository messageRepository;
    private UserRepository userRepository;
    private BookRepository bookRepository;
    private RequestRepository requestRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public InitService(MessageRepository messageRepository,
                       UserRepository userRepository,
                       BookRepository bookRepository,
                       RequestRepository requestRepository,
                       PasswordEncoder passwordEncoder) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
        this.requestRepository = requestRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void init() {

        User u0 = new User(0L, "admin", passwordEncoder.encode("admin"), Type.LIBRARIAN, "Szymon", "Betlewski");
        User u1 = new User(0L, "pai", passwordEncoder.encode("pai"), Type.READER, "Marcin", "Wójcik");
        User u2 = new User(0L, "proj", passwordEncoder.encode("proj"), Type.READER, "Stanisław", "Rubaj");
        u0 = userRepository.save(u0);
        u1 = userRepository.save(u1);
        u2 = userRepository.save(u2);

        Book b1 = new Book(0L, "Harry Potter i Zakon Feniksa", "Joanne K. Rowling", "Media Rodzina", 956, "978-83-80-08240-3", u1);
        Book b2 = new Book(0L, "Harry Potter i Książę Półkrwi", "Joanne K. Rowling", "Media Rodzina", 700, "978-83-80-08242-7", null);
        Book b3 = new Book(0L, "Harry Potter i Insygnia Śmierci", "Joanne K. Rowling", "Media Rodzina", 780, "978-83-80-08244-1", null);
        Book b4 = new Book(0L, "Prowadź swój pług przez kości umarłych", "Olga Tokarczuk", "Wydawnictwo Literackie", 320, "978-83-08-06058-2", u2);
        Book b5 = new Book(0L, "Księgi Jakubowe", "Olga Tokarczuk", "Wydawnictwo Literackie", 912, "978-83-08-04939-6", null);
        Book b6 = new Book(0L, "Immunitet. Joanna Chyłka", "Remigiusz Mróz", "Czwarta Strona", 648, "978-83-79-76511-9 ", null);
        Book b7 = new Book(0L, "Kasacja. Joanna Chyłka", "Remigiusz Mróz", "Czwarta Strona", 410, "978-83-79-76248-4", null);
        Book b8 = new Book(0L, "Pan Tadeusz", "Adam Mickiewicz", "Wilga", 400, "978-83-28-06781-3", null);
        Book b9 = new Book(0L, "Ballady i romanse", "Adam Mickiewicz", "SBM", 48, "978-83-66-48228-9", null);
        Book b10 = new Book(0L, "Opus magnum C++", "Jerzy Grębosz", "Helion", 280, "978-83-28-36587-2", null);
        b1 = bookRepository.save(b1);
        b2 = bookRepository.save(b2);
        b3 = bookRepository.save(b3);
        b4 = bookRepository.save(b4);
        b5 = bookRepository.save(b5);
        bookRepository.saveAll(Arrays.asList(b6,b7,b8,b9,b10));

        Request r1 = new Request(0L, LocalDate.now().minusDays(25), LocalDate.now().plusDays(5), true, u1, b1);
        Request r2 = new Request(0L, LocalDate.now().minusDays(15), LocalDate.now().plusDays(15), true, u2, b4);
        Request r3 = new Request(0L, null, null, null, u1, b2);
        Request r4 = new Request(0L, null, null, null, u2, b5);
        requestRepository.saveAll(Arrays.asList(r1, r2, r3, r4));

        Message m1 = new Message(0L, "Wypożyczenie", "Wypożyczono książkę: Harry Potter i Zakon Feniksa. Okres wypożyczenia: 30 dni. Miłej lektury :)", LocalDateTime.now().minusDays(25), u1);
        Message m2 = new Message(0L, "Zgłoszenie", "Zgłoszono wypożyczenie książki: Harry Potter i Książę Półkrwi.", LocalDateTime.now(), u1);
        Message m3 = new Message(0L, "Wypożyczenie", "Wypożyczono książkę: Prowadź swój pług przez kości umarłych. Okres wypożyczenia: 30 dni. Miłej lektury :)", LocalDateTime.now().minusDays(15), u2);
        Message m4 = new Message(0L, "Zgłoszenie", "Zgłoszono wypożyczenie książki: Prowadź swój pług przez kości umarłych.", LocalDateTime.now().minusDays(16), u2);
        Message m5 = new Message(0L, "Zgłoszenie", "Zgłoszono wypożyczenie książki: Księgi Jakubowe.", LocalDateTime.now(), u2);
        messageRepository.saveAll(Arrays.asList(m1, m2, m3, m4, m5));
    }
}