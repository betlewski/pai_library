package com.example.demo.service;

import com.example.demo.model.Book;
import com.example.demo.model.Request;
import com.example.demo.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {

    private RequestRepository requestRepository;

    @Autowired
    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public Request findByBook(Book book) {
        return requestRepository.findByBook(book);
    }

    public List<Request> findAll() {
        return requestRepository.findAll();
    }

    public ResponseEntity<Request> save(Request request) {
        Request newRequest;
        try {
            newRequest = requestRepository.save(request);
        }
        catch(Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(newRequest, HttpStatus.OK);
    }

    public ResponseEntity<Boolean> deleteById(Long id) {
        try {
            requestRepository.deleteById(id);
        }
        catch(Exception e) {
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
